#include<stdio.h>
#include<stdlib.h>
#include<string.h> 
#include<math.h>
#include<time.h>

struct player
{
  char name[25]; 
  int score ;
  int highest ;
};

typedef struct 
{
  char name[10];
  char password[10];
  int id;
}user;

typedef struct 
{
  char subject[10];
  char grade;

}students;

int main()
{
/*    //nested loop = a loop which is inside another loop
    // note the inner loop(this loop) is executed before the outer loop
int row;
int column;
char symbol;

printf("enter number of rows:");
scanf("%d",&row);

printf("enter number of columns:"); 
scanf("%d",&column);

scanf("%s",&symbol);

printf("enter symbol:"); 
scanf("%s", &symbol);

for (int i = 1; i <= row; i++)
{
  for (int j = 1; j <= column; j++) 
   {

      //printf("%d",j);
      printf("%s",symbol);
   }
   printf("\n");
} */

//continue - skips rest of the code and forces the rest iteration of the loop
//break - exits a loop/switch
/*
for (int i = 0; i <= 20; i++)
{
  if ( i == 13)
  {
    //continue;
    break;
  }
  printf("%d\n",i);
}
*/
 // array - is a data structure  that can store many values of te same data type

/*double prices[6]= { 5.0, 10.0, 15.0}; 
 
prices[4]= 20.0;
prices[5]= 25.0;
prices[6]= 30.0;
prices[7]= 35.0;
prices[8]= 40.0;

printf("the price is:%.2lf\n",prices[0]);
printf("the price is:%.2lf\n",prices[8]);
*/
/*
double prices[]= { 5.0, 10.0, 15.0, 20.0, 25.0}; 
int prime[]= {2.0,3.0,5.0,7.0,11.0};

//printf("%.2d bytes\n",sizeof(prime)); //shows size of operator
    //int i=0 //show which variable is to be repeated-[0],[1]
    //i < #(4,5) //shows how many times the loop must occur
    //i++ // makes the loop to proceed to the next variable 
for (int i = 0; i < sizeof(prices)/sizeof(prices[0]); i++)
{
  printf("$%.2lf\n",prices[i]);
}*/

/*
int number[3][4]; //= { {1, 2, 3} , {4, 5, 6}, {7, 8, 9} };
//printf("%d",number[2][2]);

number [0][0]= 1;
number [0][1]= 2;
number [0][2]= 3;
number [1][0]= 4;
number [1][1]= 5;
number [1][2]= 6;
number [2][0]= 7;
number [2][1]= 8;
number [2][2]= 9;

int rows = sizeof(number)/sizeof(number[0]);
int columns = sizeof(number[0])/sizeof(number[0][0]);

printf("rows= %d\n",rows);
printf("columns= %d\n",columns);

for ( int i = 0; i < rows ; i++)
{
  for ( int j = 0; j < columns ; j++)
  {
    printf("%d\t",number[i][j]);
  }
  printf("\n");
}

for ( int i = 0; i < 2 ; i++)
{
  for ( int j = 0; j < 3 ; j++)
  {
    printf("%d\t",number[i][j]);
  }
}
*/
  //char mark[6]= "bro ";
  //printf("%d",sizeof(mark));
/*
  char names[][10] = {"mark","the goat","kariuki"};

  strcpy(names[1], "kungu");//used to replace element in an array of strings 

  for(int i = 0; i < sizeof(names)/sizeof(names[0]); i++)
  {
    printf("%s\t",names[i]);
  }*/
 
  /*  char x[15] = "X";
    char y[15] = "Y";
    char temp[15] ;

    strcpy(temp,x);
    strcpy(x,y);
    strcpy(y,temp);

    printf("x= %s\n",x);
    printf("y= %s\n",y);
*/
/*
char a='A';
char b='B';
char temp;

temp = a;
a = b;
b = temp;

printf("a = %c\n",a);
printf("b = %c\n",b);*/

// note look at sorting of arrays!!!

/*
//struct -a collection of related members(variables)
//        they can be of different data types
//        listed under one name in block of memory
//

struct player player1;
struct player player2;

strcpy(player1.name , "mark");
player1.score = 81;
player1.highest= 88;

strcpy(player2.name , "goon");
player2.score = 55;
player2.highest= 74;

printf("name: %s\n",player1.name);
printf("score: %d\n",player1.score);
printf("highest : %d\n",player1.highest);

printf("name: %s\n",player2.name);
printf("score: %d\n",player2.score);
printf("highest : %d\n",player2.highest);
*/

/*
//typedef - gives an existing data type a nikname

user user1= {"artistic", "art101", 256};
user user2= {"goon", "bigthief", 1};

printf("%s\n",user1.name);
printf("%s\n",user1.password);
printf("%d\n",user1.id);
printf("\n");
printf("%s\n",user2.name);
printf("%s\n",user2.password);
printf("%d\n",user2.id);
*/
/*
students mark = {"math", 'A'};
students brad = {"math", 'D'};
students sam = {"math", 'B'};

students results[] = {mark, brad, sam};
    
for(int i =0;i < sizeof(results)/sizeof(results[0]); i++)
{
  printf("%-7s\t",results[i].subject);
  printf("%c\n",results[i].grade);
}
*/
/*
enum months{jan= 1, feb= 2, mar= 3, apr= 4, may= 5, jun= 6};
int date= feb;
if(date <=feb)
{
  printf("happy new year\n");
}else
{
  printf("normal month\n");
}
//pseude random number - are a set of values or element that are statistically random
srand(time(0));

int num1= rand()%6;
int num2= (rand()%3)+1;
int num3= rand()%200;

printf("%d\n",num1);
printf("%d\n",num2);
printf("%d\n",num3);
*/

return 0; 
}
    
