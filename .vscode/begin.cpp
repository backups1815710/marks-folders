#include <iostream>
#include <cmath>
#include <ctime>

namespace first { //used to avoid naming conflicts
    int x= 10;
} 
namespace second{
    std::string name="kungu";
}

typedef int i_t;
using char_t = std::string;

int find_num(int array[],int size,int num);
int sort(int array, int temp,int size_1);
int main()
{
   /* std::cout << "i am mark," << '\n';
    std::cout << "but u can call me kungu" << std::endl;
*/
   
   /* char_t name= "ben"; 
    i_t age= 10;
    char grade= 'B';
    double weight= 6.98; */

    //std::cout << "my name is " << name << '\n'; //using namespace second;
    //std::cout << "i am "<< first::x << " years old" << '\n';
   /* std::cout << "i got a " << grade << " in kcse";
    std::cout << '\n' << "i am " << weight << " kilos";*/

    /*std::cout << name << '\t';
    std::cout << age; */ 
// type conversion = canverting the value of a data type to another value
  /*int pie= 3.14   //implicit
    double pie= 3.14;   //explicit  
    std::cout << (int)pie; */

  /* 
   std::string name;
   int marks, age;

   std::cout << "enter marks and age: ";
   std::cin >> marks >> age;

   std::cout << "enter students full names: ";
   std::getline(std::cin >> std::ws, name); 

   std::cout << '\n'<< name << " has " << marks << " marks and is " << age << " yrs old";
*/
   double a= 5;
   double b= 4;
   double c,base, hyp;

   //c= std::max(a,b); //std::min =to find smaller
   //c=pow(a,b);
   //c= sqrt(b); //ceil,floor,round,abs
   //std::cout << c; 

/*    std::cout << "enter base and hypotenuse: ";
    std::cin  >> base >> hyp;
    c= sqrt(pow(hyp,2)-pow(base,2));
    std::cout << "ans= " << c; 
*/
    //***ternary operator***
    //condition ? expression1 :expression2
   /* bool rainy= true;
    std::cout << (rainy == true ? "the weather is rainy": "the weather is sunny") ;*/

/*
    std::cout << "******** temp_convertion ********\n";
    char unit;
    int temp;
    std::cout << " f = fahrenheit\t c = celcius \n";
    std::cout << "enter units of choice: "; 
    std::cin >> unit;
    
    if(unit == 'f')
    {
        std::cout << "enter temp: ";
        std::cin >> temp;
        temp= (temp-32)*5/9;
        std::cout << "temperature= " << temp << " c\n";
    }else if(unit == 'c')
    {
        std::cout << "enter temp: ";
        std::cin >> temp;
        temp= (temp*9/5)+32;
        std::cout << "temperature= " << temp << " f\n";
    }else
    {
        std ::cout << "enter valid unit\n";
    }
    std::cout << "*********************************";
    */

   /*std::string name;
   std::cout << "enter username: ";
   std::getline(std::cin >> std::ws,name);

   if(name.length()>8)//used to indicate length of characters
   {
    std::cout << "the name is too long\n";
   }else if (name.empty())//acts as a boolean to see if a variable is empty
   {
    std::cout << "please enter username!!!\n";
   }/*else if(name.find(' '))//used to look for characters
   {
    std::cout << "no white spaces\n";
    std::cout << name.find(' ');
     name.clear();//used to clear contents of variable
   }*/
   /*else
   { 
    name.insert(0, "NRG_");//used to insert characters to certain section of string
    std::cout << "welcome " << name << '\n';
    name.append("@gmail.com");
    std::cout << "ur email: " << name << '\n';
   }
   */

 /* srand(time(NULL));
  int number= (rand()%10)+1;

  int choice;

  do
  {
    std::cout << "enter number(1-10): ";
    std::cin >> choice;
    
    if(choice > number)
    {
        std::cout << "too high\n";
    }else if(choice < number)
    {
        std::cout << "too low\n";
    }else
    {
        std::cout << "you win\n";
    }

  } while (choice != number);*/
   
//FOR EACH LOOP=loop that eases traversal over an iterable data set
//              less syntax but less flexibility
/*
std::string crib[]= {"mark", "kamal", "eddie", "ousley", "eugene", "ago", "wayne", "brad"};
for(std::string member : crib)
{
    std::cout << member << '\t';
}
 */
//WHEN USING A ARRAY TO A FUNTION IT DECAYS TO A POINTER SO CONSIDER USING A VARIABLE THAT STORES THE NO. OF VALUES
/*
int array[]= {0,1,2,3,4,5,6,7,8,9};
int size= sizeof(array)/sizeof(array[0]);
int num_found,num_to_find;

std::cout << "enter no. to find: ";
std::cin >> num_to_find;

num_found= find_num(array, size, num_to_find);//WHEN ASSINGING ARRAY AS AN ARGUMENT THE ( [] ) IS NOT NEEDED

if(num_found == num_to_find)
{
    std::cout << "num found";

}else{
    std::cout << "num not found";
}
*/

int array[]={7,8,1,2,6,5,3,4,0,9};
int size_1= sizeof(array)/sizeof(array[0]);
int temp;
sort(array,size_1,temp);
for (int number : array)
{
    std::cout << number << '\n';
}

    return 0;
}
int find_num(int array[] ,int size,int num)
{
    for(int i=0; i< size; i++)
    {   if(array[i] == num)
        {return i;}  }
        return -1;
}
int sort(int array, int temp, int size_1)
{
    for(int i=0;i < size_1-1;  i++)
    {
        for(int j=0; j < size_1-1-i; j++)
        {
            if(array[j] > array[j+1])
            {
               temp= array[j];
               array[j]= array[j+1];
               array[j+1]= temp;  
            }
        }
    }
}