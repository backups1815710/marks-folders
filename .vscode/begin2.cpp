#include <iostream>
#include <cmath>

int main()
{
    /*
    //FILL - fill range of elements with specified value
    //          fill(begin, end, value)
 
    const int SIZE = 9 ;
    std::string foods[SIZE];

    fill(foods , foods + (SIZE/3), "ugali"); 
    fill(foods+(SIZE/3), foods+(SIZE/3*2), "chapo" );

    for(std::string food : foods)
    {
        std::cout << food << '\n' ;
    }

//memory operator "&" - shows location of certain value
std::cout << &SIZE;
*/

//pointer-a variable that stores memory address for another varieble
//          sometimes it is easier to work with than address
//* dereference operator
//NOTE=an array is already a memory address
std::string jina = "mark";
std::string *pJina = &jina;
std::cout << pJina << '\n'; 
std::cout << *pJina; //* used to show what is stored in the memory address

//null value- a special value that ,eans something has no value
//when a pointer has a null value it means that pointer is not pointing to anything
//nullptr - a keyword representing a null pointer literal
}