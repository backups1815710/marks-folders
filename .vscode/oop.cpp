#include <iostream>
class abstractemployee{
   virtual void askforpromo()=0;
};

class employee:abstractemployee{//act as a blueprint
private:
 std::string name;
 std::string company;
 int age;

public:
 void intro()
 {
    std::cout << "name= "<< name <<std::endl;
    std::cout << "company= "<< company <<std::endl ;
    std::cout << "age= "<< age <<std::endl ;
 }
 employee( std::string Name, std::string Company, int Age)
 {
    name= Name;
    company= Company;
    age= Age;
 }

 void setname(std::string namee)
 {
    name = namee;
 }
 std::string getname()
 {
     return name;
 }
 void setage(int agee)
 {
    age = agee;
 }
 int getage()
 {
    return age;
 }

 void askforpromo(){
   if(age> 30)
   {std::cout << name << "got a promotion"<< std::endl;}
   else
   {std::cout << name << " got no promotion"<< std::endl ;}
 }
};
int main()
{
   std::string jina ="james";
     employee employee1=employee("mark", "jkuat", 34);
     //employee1.age= 12;
     //employee1.company= "jkuat";
     //employee1.name= "mark";
    employee1.intro();
    
    employee cashier=employee("kungu","naivas",38);//constucts the object 
    //cashier.age= 38;
    //cashier.name= "kungu"; 
    //cashier.company= "naivas";
    cashier.intro();

   // std::cout << cashier.name << " is " << cashier.age <<" yrs old\n";
  cashier.setname(jina);//cashier.setname("james");
  cashier.setage(21);
  std::cout << cashier.getname() << " is " << cashier.getage() << " years old"<< std::endl;

  cashier.askforpromo();
  employee1.askforpromo();

  
}