#include <iostream>
#include <cmath>

class time{
    private:
    int hours,minutes;

    public:
    void set_time(int a,int b)
    {
        hours= a;
        minutes= b;
    };

    void show_time(void);
    void add_time(time t1, time t2)
    {
        minutes= t1.minutes + t2.minutes;
        hours= minutes/60;
        minutes= minutes%60;
        hours= hours+t1.hours+t2.hours;

    }
};

void time::show_time(void)
{
    std::cout << "hours:" << hours << '\n';
    std::cout << "minutes:" << minutes << std::endl ;
}

class shape
{
    protected:
    int width,length;
    public:
    void set_values(int x,int y);
};

void shape::set_values(int x ,int y)
{
    width= x;
    length= y;
}

class rectangle :public shape
{
    public:
    int area()
    {return (length*width);}
};
class triangle : public shape
{
    public:
    int area()
    {return (0.5*width*length);}
};

double area(double x)
{
    return (M_PI*x*x);
}
int area(int x,int y)
{
    return (x*y);
}
 class semester2;
class semester1
{
    float programming1,database1;
    public:
    int set_values(float x, float y)
    {
        programming1= x;
        database1= y;
    }

    friend float average(semester1,semester2);
};
class semester2
{
    float programming2,database2;
    public:
    void set_values(float x, float y)
    {
        programming2= x;
        database2= y;
    }

    friend float average(semester1,semester2);
};

float average(semester1 a,semester2 b)
{
    float x= (a.programming1+a.database1+b.programming2+b.database2)/4;
    return (x);
}

int main()
{
   /* time T1,T2,T3;
    T1.set_time(4,50);
    T2.set_time(6,40);
    T3.add_time(T1,T2);
    T1.show_time();
    T2.show_time();
    T3.show_time();
*/
/* rectangle rect1;
    rect1.set_values(10,5);
    std::cout << "the area of rectangle:" << rect1.area() << std::endl;
    triangle tri1;
    tri1.set_values(10,5);
    std::cout << "the area of triangle:" << tri1.area() << '\n';
  */
 /*
    std::cout << "the area of circle:" << area(21.12)<< '\n'; 
    std::cout << "the area of square:" << area(20,10) << '\n';
    std::cout << "the area of rectangle:" << area(5,40) << '\n';
   */
  semester1 mark;
  semester2 kungu;
    mark.set_values(30.0,40.0);
    kungu.set_values(50.0,29.0);
    std::cout << "the average is:" << average(mark,kungu)<< std::endl;
    return 0;
}
